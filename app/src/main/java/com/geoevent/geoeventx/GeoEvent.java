package com.geoevent.geoeventx;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by johan on 2018-06-26.
 */

public class GeoEvent {
    public GeoEvent(Long id, String eventName, String description, double longitude, double latitude, int radius, Date time, int timeSpanBefore, int timeSpanAfter) {
        this.id = id;
        this.eventName = eventName;
        this.description = description;
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.time = time;
        this.timeSpanBefore = timeSpanBefore;
        this.timeSpanAfter = timeSpanAfter;
    }

    private Long id;
    private String eventName;
    private String description;
    private Long originalGeoStampId;

    // GPS-Coordinates
    private double longitude;
    private double latitude;
    private int radius;
    private Date time;
    private int timeSpanBefore;
    private int timeSpanAfter;

    private boolean showAllInfo = false;
    private boolean selfCreated = true;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormatOutput;

    public boolean isShowAllInfo() {
        return showAllInfo;
    }

    public void setShowAllInfo(boolean showAllInfo) {
        this.showAllInfo = showAllInfo;
    }

    public GeoEvent(Long id, String eventName, String description, double longitude, double latitude, int radius, String time, int timeSpanBefore, int timeSpanAfter, Long originalGeoStampId, boolean selfCreated) {
        this.id = id;
        this.eventName = eventName;
        this.description = description;
        this.originalGeoStampId = originalGeoStampId;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss");
        dateFormatOutput = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        try {
            this.time = dateFormat.parse(time);
        } catch (ParseException e) {
            Log.d("GEO", "Error parsing time geoEvent class.");
            e.printStackTrace();
        }
        this.longitude = longitude;
        this.latitude = latitude;
        this.radius = radius;
        this.timeSpanBefore = timeSpanBefore;
        this.timeSpanAfter = timeSpanAfter;
        this.selfCreated = selfCreated;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getTimeSpanBefore() {
        return timeSpanBefore;
    }

    public void setTimeSpanBefore(int timeSpanBefore) {
        this.timeSpanBefore = timeSpanBefore;
    }

    public int getTimeSpanAfter() {
        return timeSpanAfter;
    }

    public void setTimeSpanAfter(int timeSpanAfter) {
        this.timeSpanAfter = timeSpanAfter;
    }

    public String timeToString() {
        return dateFormatOutput.format(this.time);
    }

    public Long getOriginalGeoStampId() {
        return originalGeoStampId;
    }

    public void setOriginalGeoStampId(Long originalGeoStampId) {
        this.originalGeoStampId = originalGeoStampId;
    }

    public boolean isSelfCreated() {
        return selfCreated;
    }

    public void setSelfCreated(boolean selfCreated) {
        this.selfCreated = selfCreated;
    }

    @Override
    public String toString() {
        return "GeoEvent{" +
                "id=" + id +
                ", eventName='" + eventName + '\'' +
                ", description='" + description + '\'' +
                ", originalGeoStampId=" + originalGeoStampId +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", radius=" + radius +
                ", time=" + time +
                ", timeSpanBefore=" + timeSpanBefore +
                ", timeSpanAfter=" + timeSpanAfter +
                ", showAllInfo=" + showAllInfo +
                ", selfCreated=" + selfCreated +
                ", dateFormat=" + dateFormat +
                ", dateFormatOutput=" + dateFormatOutput +
                '}';
    }
}




