package com.geoevent.geoeventx;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by johan on 2018-05-17.
 */

public class PersonalInfo {
    String userName;
    String email;
    Boolean loggedIn;

    PersonalInfo() {
        this.loggedIn = false;
        this.userName = "";
        this.email = "";
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(Boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void parse(String jsonString) {
        try {
            JSONObject jData = new JSONObject(jsonString);
            userName = jData.getString("userName");
            email = jData.getString("email");
        } catch (JSONException e) {
            Log.d("TAG","Cant get userdata from Json string.");
        }
    }

    @Override
    public String toString() {
        return "Username: " + this.userName + "\nEmail: " + this.email + "\nLogged in: " + (this.loggedIn?"yes":"no");
    }
}
