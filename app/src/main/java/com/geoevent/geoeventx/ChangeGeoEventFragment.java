package com.geoevent.geoeventx;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.UnsupportedEncodingException;
import java.net.Authenticator;
import java.net.PasswordAuthentication;


public class ChangeGeoEventFragment extends Fragment {
    TextView radiusValue, timeAfterValue, timeBeforeValue;
    SeekBar radius,timeBeforeSeekBar, timeAfterSeekBar;
    EditText eventName, description;
    TextView longitudeValue,latitudeValue;
    CardView confirmButton,cancelButton;

    public ChangeGeoEventFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_change_geo_event, container, false);

        GeoEvent geoEvent = new GeoEvent(
                getArguments().getLong("id"),
                getArguments().getString("eventName"),
                getArguments().getString("description"),
                getArguments().getDouble("latitude"),
                getArguments().getDouble("longitude"),
                getArguments().getInt("radius"),
                getArguments().getString("time"),
                getArguments().getInt("timeSpanBefore"),
                getArguments().getInt("timeSpanAfter"),
                getArguments().getLong("originalGeoStampId"),
                true);

        Log.d("GEO", "ChangeGeoEvent debug: " + geoEvent.toString());

        eventName = v.findViewById(R.id.eventNameInput);
        description = v.findViewById(R.id.eventDescriptionInput);
        radiusValue = v.findViewById(R.id.radiusValue);
        timeAfterValue = v.findViewById(R.id.timeAfterValue);
        timeBeforeValue = v.findViewById(R.id.timeBeforeValue);

        radius = v.findViewById(R.id.radiusSeekBar);
        timeAfterSeekBar = v.findViewById(R.id.timeAfterSeekBar);
        timeBeforeSeekBar = v.findViewById(R.id.timeBeforeSeekBar);

        longitudeValue = v.findViewById(R.id.longitudeValue);
        latitudeValue = v.findViewById(R.id.latitudeValue);
        cancelButton = v.findViewById(R.id.cancelGeoEventButton);
        confirmButton = v.findViewById(R.id.confirmGeoEventButton);

        longitudeValue.setText("" + geoEvent.getLongitude());
        latitudeValue.setText("" + geoEvent.getLatitude());
        eventName.setText(geoEvent.getEventName());
        description.setText(geoEvent.getDescription());

        radius.setProgress(geoEvent.getRadius()/100);
        timeBeforeSeekBar.setProgress(geoEvent.getTimeSpanBefore()/60);
        timeAfterSeekBar.setProgress(geoEvent.getTimeSpanAfter()/60);

        timeBeforeValue.setText("-" + timeBeforeSeekBar.getProgress() + "h");
        timeAfterValue.setText("+" + timeAfterSeekBar.getProgress() + "h");

        radiusValue.setText(radius.getProgress()*100 + "m");

        confirmButton.setOnClickListener(v1 -> {
            // Send geoEvent to server to update or create
            // Send to server
            JSONObject jOut = new JSONObject();
            try {
                if(geoEvent.getId()!=-1L) jOut.put("id",geoEvent.getId());
                jOut.put("eventName", StringEscapeUtils.escapeJava(eventName.getText().toString()).substring(0, Math.min(eventName.getText().toString().length(), 50)));
                jOut.put("description",StringEscapeUtils.escapeJava(description.getText().toString()));
                jOut.put("latitude",geoEvent.getLatitude());
                jOut.put("longitude", geoEvent.getLongitude());
                jOut.put("radius", radius.getProgress()*100);
                jOut.put("timeSpanBefore",timeBeforeSeekBar.getProgress()*60);
                jOut.put("timeSpanAfter", timeAfterSeekBar.getProgress()*60);
                jOut.put("originalGeoStampId",geoEvent.getOriginalGeoStampId());
                jOut.put("selfCreated", geoEvent.isSelfCreated());

            } catch (JSONException e) {
                Log.d("GEO","Error parsing new geoEvent before server.");
                e.printStackTrace();
            }

            Log.d("GEO","Sent to server, new geoevent: " + jOut.toString());

            new RestPost((String result) -> {
                Log.d("GEO","Output RestPost, New Geostamp: " + result);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GeoEventFragment(), "FRAG_GEOEVENTS").commit();
            }).execute("http://allander.duckdns.org:8080/geoevent/api/geoevents/my",jOut.toString());
        });

        cancelButton.setOnClickListener(v1 -> {
            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GeoEventFragment(), "FRAG_GEOEVENTS").commit();
        });

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                radius.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        radiusValue.setText(progress*100 + "m");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }

                });

                timeAfterSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        timeAfterValue.setText("+" + progress + "h");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                timeBeforeSeekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        timeBeforeValue.setText("-" + progress + "h");
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });
            }
        });



                // Inflate the layout for this fragment
        return v;
    }

}
