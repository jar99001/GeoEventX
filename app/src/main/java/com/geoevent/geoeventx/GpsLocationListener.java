package com.geoevent.geoeventx;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by johan on 2018-06-25.
 */

class GpsLocationListenerAsync extends AsyncTask<String, Void, String> {
    private GpsLocationListenerAsync.AsyncResponse delegate = null;
    private double longitude = 0;
    private double latitude = 0;
    private double accuracy = 500;
    private long startTime = System.currentTimeMillis();
    private GpsLocationListener gpsListener;
    private Context context;
    private LocationManager lm;

    public interface AsyncResponse {
        void processFinish(String output);
    }

    public GpsLocationListenerAsync(Context context, GpsLocationListenerAsync.AsyncResponse delegate){
        this.context = context;
        this.delegate = delegate;
    }

    @Override
    protected String doInBackground(String... params) {
        HandlerThread handlerThread = new HandlerThread("MyHandlerThread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();

        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d("GEO","Error: Cant access\ngps location listener.");
        } else {
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            gpsListener = new GpsLocationListener();
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER,0,0,gpsListener,looper);
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,0,0,gpsListener,looper);
        }


        // Check if location is ok accuracy
        try {
            while(System.currentTimeMillis() < (startTime + (60*1000)) && accuracy > 20) {
                    Thread.sleep(1000);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        lm.removeUpdates(gpsListener);

        JSONObject jObj = new JSONObject();
        try {
            jObj.put("longitude",longitude);
            jObj.put("latitude",latitude);
            jObj.put("accuracy",(int)accuracy);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jObj.toString();
    }

    private class GpsLocationListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            if(accuracy > location.getAccuracy()) {
                longitude = location.getLongitude();
                latitude = location.getLatitude();
                accuracy = location.getAccuracy();
                Log.d("GEO", "(Better)Provider: " + location.getProvider() + ", Location changed to: " + longitude + ", " + latitude + " - Acc: " + location.getAccuracy());
            } else {
                Log.d("GEO", "(Worse)Provider: " + location.getProvider() + ", Location changed to: " + location.getLongitude() + ", " + location.getLatitude() + " - Acc: " + location.getAccuracy());
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.d("GEO", "Provider Enabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.d("GEO", "Provider Enabled: " + provider);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.d("GEO", "Provider disables: " + provider);
        }
    }

    @Override
    protected void onPostExecute(String result) {
        delegate.processFinish(result);
    }
}


