package com.geoevent.geoeventx;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class EventCommunicationFragment extends Fragment {
    private ListView listView;
    private ProgressBar pB;
    private EventCommunicationAdapter eventCommunicationAdapter;
    private PersonalInformation personalInformation;
    JSONObject jOut;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        personalInformation = (PersonalInformation) context;

    }

    public EventCommunicationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_event_communication, container, false);

        Long geoEventId = getArguments().getLong("id");


        jOut = new JSONObject();
        try {
            jOut.put("id",geoEventId);
        } catch (JSONException e) {
            Log.d("GEO","Cant parse jOut in EventCommunicationFragment.java");
            e.printStackTrace();
        }

        new RestPost(output -> {
            listView = v.findViewById(R.id.commentsList);
            pB = v.findViewById(R.id.commentProgressBar);
            ArrayList<Comment> commentList = new ArrayList<>();


            try {
                JSONArray jArray = new JSONArray(output);
                for(int a=0;a<jArray.length();a++) {
                    JSONObject j = jArray.getJSONObject(a);

                    commentList.add(new
                                    Comment(j.getLong("id"),
                                    j.getString("userName"),
                                    StringEscapeUtils.unescapeJava(j.getString("comment")),
                                    geoEventId,
                                    j.getString("time")));
                }

                eventCommunicationAdapter = new EventCommunicationAdapter(getContext(), commentList);
                listView.setAdapter(eventCommunicationAdapter);
                pB.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

            } catch(Exception e) {
                System.err.println("JSONObject error: " + e);
            }

        }).execute("http://allander.duckdns.org:8080/geoevent/api/comments",jOut.toString());



        ImageView sendBtn = v.findViewById(R.id.sendButton);
        EditText commentToSend = v.findViewById(R.id.textToSend);

        sendBtn.setOnClickListener(v1 -> {
            PersonalInfo pI = personalInformation.getPersonalInfo();

            jOut = new JSONObject();
            try {
                jOut.put("id", geoEventId);
                jOut.put("userName", pI.getUserName());
                jOut.put("comment",StringEscapeUtils.escapeJava(commentToSend.getText().toString()));
            } catch (JSONException e) {
                Log.d("GEO","Cant parse jOut in EventCommunicationFragment.java");
                e.printStackTrace();
            }

            new RestPost(output -> {
                Log.d("GEO","Have sent comment to server.");

                EventCommunicationFragment eventCommunicationFragment = new EventCommunicationFragment();
                Bundle args = new Bundle();

                args.putLong("id",geoEventId);
                eventCommunicationFragment.setArguments(args);

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, eventCommunicationFragment, "FRAG_EVENTCOMMUNICATION").commit();
            }).execute("http://allander.duckdns.org:8080/geoevent/api/comments/send", jOut.toString());

        });

        return v;

    }

}
