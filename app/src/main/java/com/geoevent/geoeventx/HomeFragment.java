package com.geoevent.geoeventx;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;

import static android.content.Context.LOCATION_SERVICE;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    ProgressBar geoStampProgress;
    Vibrator vib;
    Ringtone ringTone;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        CardView geoStampBtn = v.findViewById(R.id.getLocationButton);
         geoStampProgress = v.findViewById(R.id.progressBar1);

        vib = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        ringTone = RingtoneManager.getRingtone(getActivity().getApplicationContext(), notification);

        geoStampBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Check for permission for gps access.
                ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.ACCESS_FINE_LOCATION},1);

                geoStampProgress.setVisibility(View.VISIBLE);
                vib.vibrate(250);
                new GpsLocationListenerAsync(getContext(),(String output) -> {
                    // Post execution code
                    Log.i("GEO","Result from GPS: " + output);

                    // Send to server
                    new RestPost((String result) -> {
                        Log.d("GEO","Output RestPost, New Geostamp: " + result);
                        geoStampProgress.setVisibility(View.INVISIBLE);
                        Log.i("TAG", "processFinished: " + output);
                        ringTone.play();
                    }).execute("http://allander.duckdns.org:8080/geoevent/api/geostamps/my",output);

                    try {
                        JSONObject jObj = new JSONObject(output);
                    } catch (JSONException e) {
                        Log.d("GEO","Error handling JSON after GpsLocationListenerAsync.");
                        e.printStackTrace();
                    }

                }).execute();
            }
        });



        return v;
    }

}
