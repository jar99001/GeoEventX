package com.geoevent.geoeventx;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;


class GeoEventAdapter extends ArrayAdapter<GeoEvent> {
    private Context context;
    private List<GeoEvent> geoEventList = new ArrayList<>();


    public GeoEventAdapter(@NonNull Context context, ArrayList<GeoEvent> list) {
        super(context, 0, list);
        this.context = context;
        this.geoEventList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem==null)
            listItem = LayoutInflater.from(this.context).inflate(R.layout.geo_event_item,parent,false);


        GeoEvent currentGeoEvent = geoEventList.get(position);

        ImageView delete = listItem.findViewById(R.id.iv_delete_event);
        ImageView change = listItem.findViewById(R.id.iv_change_geoevent);
        ImageView communicate = listItem.findViewById(R.id.iv_comment);

        TextView geoEventHeadline = listItem.findViewById(R.id.geoEventHead);
        TextView geoEventData = listItem.findViewById(R.id.geoEventData);
        TextView geoEventDescription = listItem.findViewById(R.id.geoEventDescription);
        LinearLayout geoInfoGroup  = listItem.findViewById(R.id.geoEventGroup);

        geoEventDescription.setVisibility(View.GONE);
        geoEventData.setVisibility(View.GONE);
        geoEventHeadline.setTextColor(currentGeoEvent.isSelfCreated()?Color.MAGENTA:Color.DKGRAY);
        geoEventHeadline.setText(currentGeoEvent.getEventName() + "\n" + currentGeoEvent.timeToString());

        if(currentGeoEvent.isSelfCreated()) {
            change.setOnClickListener(v -> {
                ChangeGeoEventFragment changeGeoEventFragment = new ChangeGeoEventFragment();
                Bundle args = new Bundle();

                args.putLong("id",currentGeoEvent.getId());
                args.putLong("originalGeoStampId",currentGeoEvent.getOriginalGeoStampId());
                args.putString("eventName",currentGeoEvent.getEventName());
                args.putString("description",currentGeoEvent.getDescription());
                args.putString("time",currentGeoEvent.timeToString());
                args.putDouble("latitude",currentGeoEvent.getLatitude());
                args.putDouble("longitude",currentGeoEvent.getLongitude());
                args.putInt("radius",currentGeoEvent.getRadius());
                args.putInt("timeSpanBefore",currentGeoEvent.getTimeSpanBefore());
                args.putInt("timeSpanAfter",currentGeoEvent.getTimeSpanAfter());

                changeGeoEventFragment.setArguments(args);
                ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction()./*remove(old_fragment).*/replace(R.id.fragment_container, changeGeoEventFragment,"FRAG_CHANGEGEOEVENT").commit();

            });
        } else change.setVisibility(View.INVISIBLE);

        geoInfoGroup.setOnClickListener(v -> {
            if(currentGeoEvent.isShowAllInfo()) {
                geoEventHeadline.setText(currentGeoEvent.getEventName() + "\n" + currentGeoEvent.timeToString());
                currentGeoEvent.setShowAllInfo(false);
                geoEventDescription.setVisibility(View.GONE);
                geoEventData.setVisibility(View.GONE);
            } else {
                geoEventHeadline.setText(currentGeoEvent.getEventName() + "\n" + currentGeoEvent.timeToString());

                geoEventData.setText("Lat:  " + currentGeoEvent.getLatitude()
                        + "\nLong: " + currentGeoEvent.getLongitude()
                        + "\nRadius: " + currentGeoEvent.getRadius()
                        + "\nAfter: " + currentGeoEvent.getTimeSpanAfter()/60 + "h"
                        + "\nBefore: " + currentGeoEvent.getTimeSpanBefore()/60 + "h");
//                        + "\nOrgGSid:" + currentGeoEvent.getOriginalGeoStampId()
                geoEventDescription.setText(currentGeoEvent.getDescription());
                geoEventDescription.setVisibility(View.VISIBLE);
                geoEventData.setVisibility(View.VISIBLE);
                currentGeoEvent.setShowAllInfo(true);
            }
        });

        if(currentGeoEvent.isSelfCreated()) {
            delete.setOnClickListener(v -> {
                Log.i("GEO", "Deleted item: " + position);
                new AlertDialog.Builder(v.getContext())
                        .setTitle("Removing an object!")
                        .setMessage("Sure you want to remove GeoEvent?\n" + geoEventHeadline.getText())
                        .setIcon(R.drawable.ic_delete)
                        .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                JSONObject jOut = new JSONObject();
                                try {
                                    jOut.put("id", currentGeoEvent.getId());
                                } catch (JSONException e) {
                                    Log.d("GEO", "Error parsing delete geoEvent before server.");
                                    e.printStackTrace();
                                }

                                Authenticator.setDefault(new Authenticator() {
                                    protected PasswordAuthentication getPasswordAuthentication() {
                                        return new PasswordAuthentication("johan", "r29osary".toCharArray());
                                    }
                                });

                                new RestDelete(output -> {
                                    Log.d("GEO", "Output RestDelete, New Geostamp: " + output);
                                    geoEventList.remove(currentGeoEvent);
                                }).execute("http://allander.duckdns.org:8080/geoevent/api/geoevents/my", jOut.toString());
                                Log.i("GEO", "Deleting GeoEvent!");

                                GeoEventFragment geoEventFragment = new GeoEventFragment();
                                ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, geoEventFragment, "FRAG_GEOEVENTS").commit();
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Log.i("GEO", "Cancel action: removing GeoEvent.");
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();


            });

        } else delete.setVisibility(View.INVISIBLE);


        communicate.setOnClickListener(v -> {
            EventCommunicationFragment eventCommunicationFragment = new EventCommunicationFragment();
            Bundle args = new Bundle();

            args.putLong("id",currentGeoEvent.getId());

            eventCommunicationFragment.setArguments(args);
            ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction()./*remove(old_fragment).*/replace(R.id.fragment_container, eventCommunicationFragment,"FRAG_EVENTCOMMUNICATION").commit();
        });


        return listItem;
    }
}

