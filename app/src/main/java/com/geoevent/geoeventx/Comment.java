package com.geoevent.geoeventx;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Comment {
    private Long id;
    private String userName;
    private String comment;
    private Long geoEvent;
    private Date time;

    public Comment(Long id, String userName, String comment, Long geoEvent, String time) {
        this.id = id;
        this.userName = userName;
        this.comment = comment;
        this.geoEvent = geoEvent;
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss");
        try {
            this.time = dateFormat.parse(time);
        } catch (ParseException e) {
            Log.d("GEO","Cant parse time in Comment.java!");
            e.printStackTrace();
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Long getGeoEvent() {
        return geoEvent;
    }

    public void setGeoEvent(Long geoEvent) {
        this.geoEvent = geoEvent;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }
}
