package com.geoevent.geoeventx;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.geoevent.geoeventx.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;


public class GeoStampAdapter extends ArrayAdapter<GeoStamp> {

    private Context _context;
    private List<GeoStamp> geoStampList = new ArrayList<>();

    public GeoStampAdapter(@NonNull Context context, ArrayList<GeoStamp> list) {
        super(context, 0, list);
        _context = context;
        geoStampList = list;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable final View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem==null)
            listItem = LayoutInflater.from(this._context).inflate(R.layout.geo_stamp_item,parent,false);

        GeoStamp currentGeoStamp = geoStampList.get(position);

        TextView geoStampText = (TextView) listItem.findViewById(R.id.geoStampText);
        geoStampText.setText(currentGeoStamp.getTimeStamp());

        ImageView delete = listItem.findViewById(R.id.iv_delete);
        ImageView createGeoEvent = listItem.findViewById(R.id.iv_new_geoevent);

        geoStampText.setOnClickListener(v -> {
            if(currentGeoStamp.isShowAllInfo()) {
                geoStampText.setText(currentGeoStamp.getTimeStamp());
                currentGeoStamp.setShowAllInfo(false);
            } else {
                geoStampText.setText(currentGeoStamp.getTimeStamp()
                        + "\nLat:  " + currentGeoStamp.getLatitude()
                        + "\nLong: " + currentGeoStamp.getLongitude()
                );
                currentGeoStamp.setShowAllInfo(true);
            }
        });


        delete.setOnClickListener(v -> {
            Log.i("GEO","Deleted item: " + position);
                new AlertDialog.Builder(v.getContext())
                    .setTitle("Removing an object!")
                    .setMessage("Sure you want to remove GeoStamp?\n" + geoStampText.getText())
                    .setIcon(R.drawable.ic_delete)
                    .setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            JSONObject jOut = new JSONObject();
                            try {
                                jOut.put("id", currentGeoStamp.getId());
                            } catch (JSONException e) {
                                Log.d("GEO","Error parsing delete geoEvent before server.");
                                e.printStackTrace();
                            }

                            Authenticator.setDefault(new Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication("johan", "r29osary".toCharArray());
                                }
                            });

                            new RestDelete(output -> {
                                Log.d("GEO","Output RestDelete, New Geostamp: " + output);
                                geoStampList.remove(currentGeoStamp);
                            }).execute("http://allander.duckdns.org:8080/geoevent/api/geostamps/my", jOut.toString());
                            Log.i("GEO","Deleting GeoStamp!");
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("GEO","Cancel action: removing GeoStamp.");
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
        });

        createGeoEvent.setOnClickListener(v -> {
            ChangeGeoEventFragment changeGeoEventFragment = new ChangeGeoEventFragment();
            Bundle args = new Bundle();

            args.putLong("id",-1L);
            args.putLong("originalGeoStampId",currentGeoStamp.getId());
            args.putString("eventName","");
            args.putString("description","");

            args.putString("time",currentGeoStamp.getTimeStampRaw());
            args.putDouble("latitude",currentGeoStamp.getLatitude());
            args.putDouble("longitude",currentGeoStamp.getLongitude());

            args.putInt("radius",0);
            args.putInt("timeSpanBefore",0);
            args.putInt("timeSpanAfter",0);


            changeGeoEventFragment.setArguments(args);
            //Fragment old_fragment = ((FragmentActivity)getContext()).getSupportFragmentManager().findFragmentByTag("LOGIN_FRAG");

            ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction()./*remove(old_fragment).*/replace(R.id.fragment_container, changeGeoEventFragment,"FRAG_CHANGEGEOEVENT").commit();

        });

        return listItem;
    }
}

