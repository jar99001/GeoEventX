package com.geoevent.geoeventx;

import java.sql.Time;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class GeoStamp {
    private long id;
    private double latitude;
    private double longitude;
    private String originalDate;
    private Date timeStamp;
    private DecimalFormat decimalFormat;
    private SimpleDateFormat dateFormat;
    private SimpleDateFormat dateFormatOutput;
    private boolean showAllInfo = false;

    public boolean isShowAllInfo() {
        return showAllInfo;
    }

    public void setShowAllInfo(boolean showAllInfo) {
        this.showAllInfo = showAllInfo;
    }


    public GeoStamp(long id, double latitude, double longitude, String timeStamp_) {
        this.id = id;
        this.originalDate = timeStamp_;
        dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:sss");
        dateFormatOutput = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        try {
            timeStamp = dateFormat.parse(timeStamp_);
        } catch (Exception e) {
            System.err.println("Cant parse string: " + timeStamp_);
        }

        decimalFormat = new DecimalFormat("#.00");
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }


    public String getTimeStamp() {
        return dateFormatOutput.format(timeStamp);
    }

    public String getTimeStampRaw() {
        return this.originalDate;
    }

    public String getLatitudeStr() {
        return new String(decimalFormat.format(this.latitude));
    }

    public String getLongitudeStr() {
        return new String(decimalFormat.format(this.longitude));
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

}
