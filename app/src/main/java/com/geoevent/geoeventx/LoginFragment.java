package com.geoevent.geoeventx;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;


public class LoginFragment extends Fragment  {
    final String server = "http://allander.duckdns.org:8080";
    CardView loginBtn;
    TextView test;
    ImageView loginLogo;
    ProgressBar loginProgress;
    PersonalInfo personalData;
    TextView debugOutput;
    EditText userName, password;
    Vibrator vib;
    PersonalInformation personalInfo;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        personalInfo = (PersonalInformation)context;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             final Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.login_fragment, container, false);
        loginBtn = v.findViewById(R.id.loginButton);
        vib = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        userName = v.findViewById(R.id.userNameBox);
        password = v.findViewById(R.id.passwordBox);
        loginLogo = v.findViewById(R.id.loginLogo);
        loginProgress = v.findViewById(R.id.loginProgress);

        Log.i("TAG","User: " + userName.getText().toString() + "Pass: " + password.getText().toString());
        personalData = new PersonalInfo();

        Authenticator.setDefault(new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(userName.getText().toString(),password.getText().toString().toCharArray());
            }
        });


        loginBtn.setOnClickListener(view -> {
            vib.vibrate(250);
            loginProgress.setVisibility(View.VISIBLE);

            Authenticator.setDefault(new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(userName.getText().toString(),password.getText().toString().toCharArray());
                }
            });

            new RestGet((String data) -> {
                loginLogo.setVisibility(View.INVISIBLE);

                // Get login information from server
                Log.d("GEO","Username: " + userName.getText().toString() + ", Password: " + password.getText().toString());
                Log.d("GEO","Data: " + data);

                if(data!="") {
                    try {
                        JSONObject jOut = new JSONObject(data);

                        personalInfo.setPersonalInfo(
                                jOut.getString("userName"),
                                jOut.getString("email"),
                                true);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.d("GEO","No data to parse from login");
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                    alertDialog
                            .setTitle("Failed to login to server.")
                            .setMessage("Are you sure you have entered the right username and password?")
                            .setPositiveButton("OK", (dialog, which) -> {

                    }).create().show();
                }

                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment(), "FRAG_HOME").commit();
            }).execute("http://allander.duckdns.org:8080/geoevent/api/accounts/my");
        });

        return v;
    }

}
