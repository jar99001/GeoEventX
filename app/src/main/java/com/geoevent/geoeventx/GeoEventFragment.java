package com.geoevent.geoeventx;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by johan on 2018-06-26.
 */

public class GeoEventFragment extends Fragment {
    private ListView listView;
    private ProgressBar pB;
    private GeoEventAdapter geoEventAdapter;

    public GeoEventFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_geo_events, container, false);

        new RestGet((String output) ->  {
            listView = v.findViewById(R.id.geoeventlist);
            pB = v.findViewById(R.id.geoStampProgressBar);
            ArrayList<GeoEvent> geoEventList = new ArrayList<>();

            int length = 0;
            JSONArray jArray = null;
            try {
                jArray = new JSONArray(output);
                length = jArray.length();
            } catch(Exception e) {
                System.err.println("JSONObject error: " + e);
            }
                for(int a=0;a<length;a++) {
                    try {
                        JSONObject j = jArray.getJSONObject(a);
                        geoEventList.add(new
                                GeoEvent(j.getLong("id"),
                                StringEscapeUtils.unescapeJava(j.getString("eventName")),
                                StringEscapeUtils.unescapeJava(j.getString("description")),
                                j.getDouble("longitude"),
                                j.getDouble("latitude"),
                                j.getInt("radius"),
                                j.getString("time"),
                                j.getInt("timeSpanBefore"),
                                j.getInt("timeSpanAfter"),
                                j.getLong("originalGeoStampId"),
                                j.getBoolean("selfCreated")
                                ));
                    } catch(Exception e) {
                        Log.d("GEO","Loop JSONObject error: " + e);
                    }
                }


            geoEventAdapter = new GeoEventAdapter(getContext(), geoEventList);
            listView.setAdapter(geoEventAdapter);
            pB.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);


        }).execute("http://allander.duckdns.org:8080/geoevent/api/geoevents/my");


        return v;
    }
}
