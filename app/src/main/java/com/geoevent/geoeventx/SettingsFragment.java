package com.geoevent.geoeventx;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    PersonalInformation personalInformation;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        personalInformation = (PersonalInformation) context;
    }

    public SettingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_settings, container, false);

        TextView personalInformationText = v.findViewById(R.id.personInformationText);

        PersonalInfo pI = personalInformation.getPersonalInfo();
        personalInformationText.setText("Username: " + pI.getUserName() + "\nEmail: " + pI.getEmail());

        return v;
    }

}
