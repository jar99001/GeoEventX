package com.geoevent.geoeventx;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by johan on 2018-08-06.
 */

public class MatchingGeoEventsAdapter extends ArrayAdapter<GeoEvent> {
    private Context context;
    private List<GeoEvent> geoEventList = new ArrayList<>();


    public MatchingGeoEventsAdapter(@NonNull Context context, ArrayList<GeoEvent> list) {
        super(context, 0, list);
        this.context = context;
        this.geoEventList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem==null)
            listItem = LayoutInflater.from(this.context).inflate(R.layout.matching_geo_events_item,parent,false);


        GeoEvent currentGeoEvent = geoEventList.get(position);

        ImageView connectTo = listItem.findViewById(R.id.iv_connect_to_event);

        TextView geoEventText = listItem.findViewById(R.id.geoEventText);
        geoEventText.setText(currentGeoEvent.getEventName() + "\n" + currentGeoEvent.timeToString());




        geoEventText.setOnClickListener(v -> {
            if(currentGeoEvent.isShowAllInfo()) {
                geoEventText.setText(currentGeoEvent.getEventName() + "\n" + currentGeoEvent.timeToString());
                currentGeoEvent.setShowAllInfo(false);
            } else {
                geoEventText.setText(currentGeoEvent.getEventName()
                        + "\n" + currentGeoEvent.timeToString()
                        + "\nLat:  " + currentGeoEvent.getLatitude()
                        + "\nLong: " + currentGeoEvent.getLongitude()
                        + "\nRadius: " + currentGeoEvent.getRadius()
                        + "\nBefore: " + currentGeoEvent.getTimeSpanBefore()/30 + "h"
                        + "\nAfter: " + currentGeoEvent.getTimeSpanAfter()/30 + "h"
                        + "\nOrgGSid:" + currentGeoEvent.getOriginalGeoStampId()
                        + "\n" + currentGeoEvent.getDescription()
                );
                currentGeoEvent.setShowAllInfo(true);
            }
        });

        connectTo.setOnClickListener(v -> {
            Log.i("GEO","Connecting to geoEvent av position: " + position + "as guest: ");

            new AlertDialog.Builder(v.getContext())
                    .setTitle("Connecting to GeoEvent")
                    .setMessage("Are you sure you want to connect to GeoEvent?\n" + geoEventText.getText())
                    .setIcon(R.drawable.ic_geoevents)
                    .setPositiveButton("Connect", (dialog, which) -> {

                        JSONObject jOut = new JSONObject();
                        try {
                                jOut.put("id", currentGeoEvent.getId());
                        } catch (JSONException e) {
                            Log.d("GEO","Error parsing new connectTo before sending to server.");
                            e.printStackTrace();
                        }

                        Log.d("GEO","Sending data to connectTo: " + jOut.toString());
                        new RestPost((String output)-> {
                            Log.d("GEO","Output RestPost, Connect to GeoEvent: " + output);
//                            getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GeoEventFragment(), "FRAG_GEOEVENTS").commit();
                        }).execute("http://allander.duckdns.org:8080/geoevent/api/matching/my", jOut.toString());
                        Log.d("GEO","Connecting....");
                    }).setNegativeButton("Cancel", (dialog, which) -> {
                        Log.d("GEO","Chickening out....");
                    }).create().show();

            /*
        delete.setOnClickListener(v -> {
            Log.i("GEO","Deleted item: " + position);
            new AlertDialog.Builder(v.getContext())
                    .setTitle("Removing an object!")
                    .setMessage("Sure you want to remove GeoEvent?\n" + geoEventText.getText())
                    .setIcon(R.drawable.ic_delete)
                    .setPositiveButton("Delete", new DiaDeletelogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            JSONObject jOut = new JSONObject();
                            try {
                                jOut.put("id", currentGeoEvent.getId());
                            } catch (JSONException e) {
                                Log.d("GEO","Error parsing delete geoEvent before server.");
                                e.printStackTrace();
                            }

                            Authenticator.setDefault(new Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication("johan", "r29osary".toCharArray());
                                }
                            });

                            new RestDelete(output -> {
                                Log.d("GEO","Output RestDelete, New Geostamp: " + output);
                                geoEventList.remove(currentGeoEvent);
                            }).execute("http://allander.duckdns.org:8080/geoevent/api/geoevents/my", jOut.toString());
                            Log.i("GEO","Deleting GeoEvent!");

                            GeoEventFragment geoEventFragment = new GeoEventFragment();
                            ((FragmentActivity)getContext()).getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, geoEventFragment,"FRAG_GEOEVENTS").commit();
                            dialog.dismiss();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Log.i("GEO","Cancel action: removing GeoEvent.");
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();

        */
        });

        return listItem;
    }

}
