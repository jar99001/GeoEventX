package com.geoevent.geoeventx;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class EventCommunicationAdapter extends ArrayAdapter<Comment> {
    private Context context;
    private List<Comment> commentList = new ArrayList<>();


    public EventCommunicationAdapter(@NonNull Context context, ArrayList<Comment> list) {
        super(context, 0, list);
        this.context = context;
        this.commentList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        View listItem = convertView;
        if(listItem==null)
            listItem = LayoutInflater.from(this.context).inflate(R.layout.comment_item,parent,false);

        Comment currentComment = commentList.get(position);

        TextView userName = listItem.findViewById(R.id.userNameText);
        TextView comment = listItem.findViewById(R.id.commentText);

        userName.setText(currentComment.getUserName());
        comment.setText(currentComment.getComment());

        return listItem;
    }
}
