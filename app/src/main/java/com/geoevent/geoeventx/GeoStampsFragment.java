package com.geoevent.geoeventx;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class GeoStampsFragment extends Fragment {
    private ListView listView;
    private ProgressBar pB;
    private GeoStampAdapter geoStampAdapter;

    public GeoStampsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_geo_stamps, container, false);


        // Get values from server
        new RestGet((String output) ->  {
//            String[] jsonObjects = output.split(",");
            listView = v.findViewById(R.id.geostamplist);
            pB = v.findViewById(R.id.geoStampProgressBar);
            ArrayList<GeoStamp> geoStampList = new ArrayList<>();

            try {
                JSONArray jArray = new JSONArray(output);
                for(int a=0;a<jArray.length();a++) {
                    JSONObject j = jArray.getJSONObject(a);
                    geoStampList.add(new GeoStamp(j.getLong("id"),j.getDouble("longitude"), j.getDouble("latitude"), j.getString("time")));
                }
                geoStampAdapter = new GeoStampAdapter(getContext(), geoStampList);
                listView.setAdapter(geoStampAdapter);
                pB.setVisibility(View.GONE);
                listView.setVisibility(View.VISIBLE);

            } catch(Exception e) {
                System.err.println("JSONObject error: " + e);
            }

        }).execute("http://allander.duckdns.org:8080/geoevent/api/geostamps/my");


        return v;
    }

}
