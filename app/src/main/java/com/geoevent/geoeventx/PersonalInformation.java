package com.geoevent.geoeventx;

/**
 * Created by johan on 2018-08-08.
 */

public interface PersonalInformation {
    PersonalInfo personalInfo = null;

    void setPersonalInfo(String userName, String email, Boolean loggedIn);
    PersonalInfo getPersonalInfo();
}