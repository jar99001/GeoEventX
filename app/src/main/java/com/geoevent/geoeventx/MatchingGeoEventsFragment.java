package com.geoevent.geoeventx;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class MatchingGeoEventsFragment extends Fragment {

    ListView listView;
    MatchingGeoEventsAdapter matchingGeoEventAdapter;

    public MatchingGeoEventsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_matching_geo_events, container, false);

        // Get values from server



        new RestGet((String output) ->  {
            listView = v.findViewById(R.id.matchinggeoeventlist);
            ProgressBar pB = v.findViewById(R.id.matchingGeoEventsProgressBar);
            ArrayList<GeoEvent> geoEventList = new ArrayList<>();

            if(output!="") {
                try {
                    JSONArray jArray = new JSONArray(output);
                    for (int a = 0; a < jArray.length(); a++) {
                        JSONObject j = jArray.getJSONObject(a);
                        geoEventList.add(new
                                GeoEvent(j.getLong("id"),
                                StringEscapeUtils.unescapeJava(j.getString("eventName")),
                                StringEscapeUtils.unescapeJava(j.getString("description")),
                                j.getDouble("longitude"),
                                j.getDouble("latitude"),
                                j.getInt("radius"),
                                j.getString("time"),
                                j.getInt("timeSpanBefore"),
                                j.getInt("timeSpanAfter"),
                                j.getLong("originalGeoStampId"),
                                j.getBoolean("selfCreated")
                        ));
                    }
                    matchingGeoEventAdapter = new MatchingGeoEventsAdapter(getContext(), geoEventList);
                    listView.setAdapter(matchingGeoEventAdapter);
                    pB.setVisibility(View.GONE);
                    listView.setVisibility(View.VISIBLE);

                } catch (Exception e) {
                    System.err.println("JSONObject error: " + e);
                }
            } else {
                // No data from server
                pB.setVisibility(View.GONE);
            }
        }).execute("http://allander.duckdns.org:8080/geoevent/api/matching/my");



        return v;
    }

}
