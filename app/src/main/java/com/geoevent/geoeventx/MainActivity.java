package com.geoevent.geoeventx;

import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.style.UpdateAppearance;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PersonalInformation {
    boolean user_loggedIn = false;
    long geoStampConnect_delay = 30000; // 30 sec
    long geoStampConnect_time = 0;
    ImageView loginIcon = null;
    PersonalInfo personalInfo = new PersonalInfo();

    @Override
    public void setPersonalInfo(String userName, String email, Boolean loggedIn) {
        this.personalInfo.setUserName(userName);
        this.personalInfo.setEmail(email);
        this.personalInfo.setLoggedIn(loggedIn);
        Log.d("GEO","Running Personal information interface, User: " + personalInfo.getUserName());
        if(loggedIn && loginIcon!=null) loginIcon.setImageResource(R.drawable.ic_logged_in);
    }

    @Override
    public PersonalInfo getPersonalInfo() {
        return this.personalInfo;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment(), "FRAG_LOGIN").commit();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.getHeaderView(0);
        loginIcon = headerView.findViewById(R.id.loginIcon);
        loginIcon.setImageResource(R.drawable.ic_logged_out);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.


        if(geoStampConnect_delay + geoStampConnect_time < System.currentTimeMillis()) {
            geoStampConnect_time = System.currentTimeMillis();


//            Ringtone ringTone;
//            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            ringTone = RingtoneManager.getRingtone(getApplicationContext(), notification);
//            ringTone.play();
        }

        int id = item.getItemId();

        // REMOVE!!!!
//        user_loggedIn = true;

        if(!personalInfo.getLoggedIn()) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment(), "FRAG_LOGIN").commit();
        } else if (id == R.id.nav_home) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment(), "FRAG_HOME").commit();
        } else if (id == R.id.nav_geostamps) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GeoStampsFragment(), "FRAG_GEOSTAMPS").commit();
        } else if (id == R.id.nav_geoevents) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new GeoEventFragment(), "FRAG_GEOEVENTS").commit();
        } else if (id == R.id.nav_matchinggeoevents) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new MatchingGeoEventsFragment(), "FRAG_MATCHINGGEOEVENTS").commit();
        } else if (id == R.id.nav_settings) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new SettingsFragment(), "FRAG_SETTINGS").commit();
        } else if (id == R.id.nav_logout) {
            personalInfo.setLoggedIn(false);
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new LoginFragment(), "FRAG_LOGIN").commit();
        } else if(id == R.id.nav_email) {
                Intent intent = new Intent(Intent.ACTION_SENDTO);
                intent.setData(Uri.parse("mailto:root@geoevent.eu"));
                intent.putExtra(Intent.EXTRA_SUBJECT, "I have a problems with you crap-app!");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
